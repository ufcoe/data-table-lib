import {Component, Directive, Input, NgModule} from '@angular/core';

@Component({selector: 'mat-autocomplete', template: ''})
export class MatAutocompleteStubComponent {
  @Input() displayWith;
}

@Component({selector: 'mat-card', template: ''})
export class MatCardStubComponent {}

@Component({selector: 'mat-card-content', template: ''})
export class MatCardContentStubComponent {}

@Component({selector: 'mat-form-field', template: ''})
export class MatFormFieldStubComponent {}

@Component({selector: 'mat-option', template: ''})
export class MatOptionStubComponent {
  @Input() value;
}


@Component({selector: 'mat-toolbar', template: ''})
export class MatToolbarStubComponent {}

@Component({selector: 'mat-paginator', template: ''})
export class MatPaginatorStubComponent {
  @Input() length;
  @Input() pageIndex;
  @Input() pageSize;
  @Input() pageSizeOptions;
}


@Directive({selector: '[matCellDef]'})
export class MatCellDefStubDirective {}

@Directive({selector: '[mat-cell]'})
export class MatCellStubDirective {}

@Directive({selector: '[matColumnDef]'})
export class MatColumnDefStubDirective {
  @Input('matColumnDef') name;
}

@Directive({selector: '[mat-header-cell]'})
export class MatHeaderCellStubDirective {}

@Directive({selector: '[matHeaderRowDef]'})
export class MatHeaderRowDefStubDirective {
  @Input() matHeaderRowDef;
}

@Directive({selector: '[matInput'})
export class MatInputStubDirective {}

@Directive({selector: '[matRowDef]'})
export class MatRowDefStubDirective {
  @Input() matRowDefColumns;
}

@Directive({selector: '[mat-sort-header]'})
export class MatSortHeaderStubDirective {}

@Directive({selector: '[mat-table]'})
export class MatTableStubDirective {
  @Input() dataSource: any;
}




@NgModule({
  declarations: [
    MatAutocompleteStubComponent,
    MatCardStubComponent,
    MatCardContentStubComponent,
    MatCellDefStubDirective,
    MatCellStubDirective,
    MatColumnDefStubDirective,
    MatFormFieldStubComponent,
    MatHeaderCellStubDirective,
    MatHeaderRowDefStubDirective,
    MatInputStubDirective,
    MatOptionStubComponent,
    MatPaginatorStubComponent,
    MatRowDefStubDirective,
    MatSortHeaderStubDirective,
    MatTableStubDirective,
    MatToolbarStubComponent
  ],
  exports: [
    MatAutocompleteStubComponent,
    MatCardStubComponent,
    MatCardContentStubComponent,
    MatCellDefStubDirective,
    MatCellStubDirective,
    MatColumnDefStubDirective,
    MatFormFieldStubComponent,
    MatHeaderCellStubDirective,
    MatHeaderRowDefStubDirective,
    MatInputStubDirective,
    MatOptionStubComponent,
    MatPaginatorStubComponent,
    MatRowDefStubDirective,
    MatSortHeaderStubDirective,
    MatTableStubDirective,
    MatToolbarStubComponent
  ]
})
export class MaterialStubsModule {}
