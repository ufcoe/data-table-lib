import {NgModule} from '@angular/core';
import {RouterLinkStubDirective} from './router-link-directive-stub';
import {FormsModule} from '@angular/forms';
import {DCDMaterialModule} from '../lib/dcdmaterial.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@NgModule(
  {
    imports: [
      DCDMaterialModule,
      FormsModule,
      NoopAnimationsModule
    ],
    declarations: [
      RouterLinkStubDirective
    ],
    exports: [
      FormsModule,
      DCDMaterialModule,
      NoopAnimationsModule,
      RouterLinkStubDirective
    ]
  }
)
export class TestingModule {}
