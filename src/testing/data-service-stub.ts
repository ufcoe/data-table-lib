import {DataService} from '../lib/data.service';
import {DCApiOperation, DCDataModel, DCOpStatus, DCSerializable, IDCDataExchange} from '@devctrl/data-model';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Machine, IPAddress, VMHost} from '@ufcoe/puters-model';


const serverData =  {
  'Machine': [
    {
      '_id': 'dfcd27ac-261b-4702-8ec7-08a9b0cfaafb',
      'name': 'mysql-47',
      'os': 'Snarky Snitch',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f'
    }, {
      '_id': '1dd3a1fe-98d2-46b8-a061-3654a856c473',
      'name': 'mysql-archive-53',
      'os': 'Ubuntu 14.04',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f',
      'IPAddresses': []
    }, {
      '_id': '96f9646d-89cd-4deb-8ec0-8252730e6a3c',
      'name': 'coe-sql',
      'os': 'Windows 31',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f',
      'IPAddresses': []
    }, {
      '_id': 'aab98ff7-910e-4736-9e87-2a26216afb0e',
      'name': 'mysql-archive-52',
      'os': 'Ubuntu 14.04',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f',
      'IPAddresses': []
    }, {
      '_id': 'ac17945a-7162-4e16-bb42-71b79e35c242',
      'name': 'Machine 1',
      'os': 'Bifurcated Bitumin',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f',
      'IPAddresses': []
    }, {
      '_id': 'fa7eafa2-badb-433a-a3a5-f78f23249a9b',
      'name': 'Machine 4',
      'os': 'Titular Tong',
      'vmHostId': 'de340829-ac64-433a-80a5-0660bbbac37f',
      'IPAddresses': []
    }
  ],
  'VMHost': [{'_id': 'de340829-ac64-433a-80a5-0660bbbac37f', 'name': 'Host 1', 'description': 'The first VM Host'}],
  'IPAddress': [{
    '_id': '25063713-c4ac-4453-a177-59bc35e67892',
    'name': '10.227.110.53',
    'machineId': 'dfcd27ac-261b-4702-8ec7-08a9b0cfaafb',
    'hostnames': []
  }]
};


export class DataServiceStub extends DataService {
  protected throwErrors = true;

  static getSampleItemObservable(): Observable<DCSerializable> {
    const model = new DCDataModel([Machine, VMHost, IPAddress]);
    const machine = new Machine(serverData['Machine'][0], model, Machine.tableName);
    machine.tableName = 'Machine';
    machine.IPAddresses = [ new IPAddress(serverData['IPAddress'][0], model, IPAddress.tableName)];
    machine.IPAddresses[0].tableName = 'IPAddress';
    return of(machine);
  }

  constructor() {
    super({
      websocketUrl: 'http://thiswontwork',
      modelList: [],
      modelVersion: "6.9",
    });
  }


  sendExchange(obj: IDCDataExchange): Observable<IDCDataExchange> {
    this.activeRequests[obj._id] = new BehaviorSubject<IDCDataExchange>(obj);

    setTimeout(() => {
      // Process the request with mock data
      switch (obj.op) {
        case (DCApiOperation.add):
        case (DCApiOperation.delete):
        case (DCApiOperation.update):
          obj.status = DCOpStatus.completed;
          this.processServerData(JSON.stringify(obj));
          break;
        case (DCApiOperation.get):
        case (DCApiOperation.search):
          // Only load the example data once it has been requested
          obj.status = DCOpStatus.completed;
          obj.op = DCApiOperation.add;
          obj.data = serverData;
          this.processServerData(JSON.stringify(obj));
      }
    });


    return this.activeRequests[obj._id];
  }


  protected socketSetup() {
    // Don't use a real socket
    this.socketIsReady = true;
  }
}
