import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {IDCTableDef} from '@devctrl/data-model';

@Component({
  selector: 'dcd-tables-list',
  templateUrl: './tables-list.component.html',
  styleUrls: ['./tables-list.component.css']
})
export class TablesListComponent implements OnInit {

  schemaArray: IDCTableDef[];

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.schemaArray = Object.keys(this.ds.schema).map(tname => this.ds.schema[tname]);
  }

}
