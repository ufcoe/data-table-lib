import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablesListComponent } from './tables-list.component';
import {DataServiceStub} from '../../testing/data-service-stub';
import {TestingModule} from '../../testing/testing-module';
import {DataService} from '../data.service';

describe('TablesListComponent', () => {
  let component: TablesListComponent;
  let fixture: ComponentFixture<TablesListComponent>;

  beforeEach(async(() => {
    const ds = new DataServiceStub();

    TestBed.configureTestingModule({
      declarations: [ TablesListComponent ],
      imports: [ TestingModule ],
      providers: [ { provide: DataService, useValue: ds}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
