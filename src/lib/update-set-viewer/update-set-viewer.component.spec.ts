import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSetViewerComponent } from './update-set-viewer.component';

describe('UpdateSetViewerComponent', () => {
  let component: UpdateSetViewerComponent;
  let fixture: ComponentFixture<UpdateSetViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSetViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSetViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
