import {Component, Input, OnInit} from '@angular/core';
import { IDCUpdateSet } from '@devctrl/data-model';

@Component({
  selector: 'dcd-update-set-viewer',
  templateUrl: './update-set-viewer.component.html',
  styleUrls: ['./update-set-viewer.component.css']
})
export class UpdateSetViewerComponent implements OnInit {

  @Input() updateSet: IDCUpdateSet = {};

  constructor() { }

  ngOnInit() {
  }

}
