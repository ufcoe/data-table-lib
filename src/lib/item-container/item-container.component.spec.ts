import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemContainerComponent } from './item-container.component';
import {Observable} from 'rxjs';
import {DCSerializable} from '@devctrl/data-model';
import {Component, Input} from '@angular/core';
import {ActivatedRouteStub} from '../../testing/activated-route-stub';
import {TestingModule} from '../../testing/testing-module';
import {DataServiceStub} from '../../testing/data-service-stub';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';

@Component({selector: 'dcd-item', template: ''})
class ItemStubComponent {
  @Input() item$: Observable<DCSerializable>;
}


describe('ItemContainerComponent', () => {
  let component: ItemContainerComponent;
  let fixture: ComponentFixture<ItemContainerComponent>;

  beforeEach(async(() => {
    const route = new ActivatedRouteStub({id: 'dfcd27ac-261b-4702-8ec7-08a9b0cfaafb'});
    const ds = new DataServiceStub();

    TestBed.configureTestingModule({
      declarations: [ ItemContainerComponent, ItemStubComponent ],
      imports: [ TestingModule],
      providers: [
        { provide: DataService, useValue: ds},
        { provide: ActivatedRoute, useValue: route}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
