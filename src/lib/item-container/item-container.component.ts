import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DCSerializable} from '@devctrl/data-model';
import {Observable, Subject} from 'rxjs';
import {DataService} from '../data.service';
import {distinctUntilKeyChanged, flatMap, map, tap, share, takeUntil, filter, startWith, shareReplay} from 'rxjs/operators';

@Component({
  selector: 'dcd-item-container',
  templateUrl: './item-container.component.html',
  styleUrls: ['./item-container.component.css']
})
export class ItemContainerComponent implements OnInit, OnDestroy {

  partialId: string;
  obj$: Observable<DCSerializable>;
  matches$: Observable<DCSerializable[]>;
  onDestroy$ = new Subject<void>();


  constructor(private ds: DataService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.matches$ = this.route.params.pipe(
      distinctUntilKeyChanged('id'),
      tap(params => this.partialId = params.id),
      flatMap( params => this.ds.getItemMatches(params.id)),
      map( matches => matches.map(id => this.ds.getItem(id))),
      shareReplay(1),
      takeUntil(this.onDestroy$),
      startWith([]),
    );

    this.obj$ = this.matches$.pipe(
      filter(matches => matches.length === 1),
      map(matches => matches[0]),
      tap( obj => console.log(`emitting obj ${obj.name}`))
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

}
