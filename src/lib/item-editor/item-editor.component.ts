import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  DCApiOperation,
  DCOpStatus,
  DCSerializable,
  IDCColumnDef,
  IDCForeignColumnDef,
  IDCReferenceColumnDef,
  IDCTableDef
} from '@devctrl/data-model';
import {DataService} from '../data.service';
import {Router} from '@angular/router';


// TODO: navigation on item deletion
// TODO: can't set foreign key to null

@Component({
  selector: 'dcd-item-editor',
  templateUrl: './item-editor.component.html',
  styleUrls: ['./item-editor.component.css']
})
export class ItemEditorComponent implements OnInit, OnDestroy {

  values = Object.values;

  @Input() newItem = false;
  @Input() item$: Observable<DCSerializable>;
  @Output() completed = new EventEmitter<any>();
  obj: DCSerializable;
  eObj: DCSerializable;
  tdef: IDCTableDef;
  errors = {};

  onDestroy$ = new Subject<void>();

  constructor(private ds: DataService, private router: Router) {

  }

  ngOnInit() {
    this.item$.subscribe( obj => {
        if (!this.obj || obj._id !== this.obj._id) {
          this.obj = obj;
          this.eObj = obj.clone();
          this.tdef = this.ds.schema[obj.tableName];
        } else {
          console.log(`ignoring update to ${obj.shortId} while editing`);
        }
      },
      () => { console.log(`observable error for item ${this.obj._id}`); },
    () => {

        // item has been deleted, go to table page
        if (! this.newItem) {
          this.completed.emit(DCApiOperation.delete);
        }
      }
    );
  }

  ngOnDestroy() {

  }

  addItem() {
    for (const col of this.columns()) {
      // Checkboxes with a null value look like they are set to false, set the value to false
      // before sending to data service
      if (col.type === 'boolean' && ! col.optional) {
        this.eObj[col.property] = !!this.eObj[col.property];
      }
    }
    try {
      this.ds.addItem(this.eObj).subscribe(
        (exch) => {
          if (exch.status === DCOpStatus.completed) {
            this.completed.emit(DCApiOperation.add);
          }
        },
        (err) => {
          this.handleError(err);
        },
        () => { // this.location.back();
        }
      );
    } catch (e) {
      this.handleError(e);
    }
  }

  cancel() {
    this.completed.emit("cancelled");
  }

  columns(): IDCColumnDef[] {
    return Object.values(this.tdef.columns);
  }

  deleteItem() {
    this.ds.deleteItem(this.obj).subscribe(
      (exch) => {
        if (exch.status === DCOpStatus.completed) {
          this.completed.emit(DCApiOperation.delete);
        }
      },
      (err) => {
        this.handleError(err);
      },
      () => {}
    );
  }

  foreignColumns(): IDCForeignColumnDef[] {
    return Object.values(this.tdef.foreignColumns);
  }

  foreignObjectSelected(obj: DCSerializable, prop: string) {
    const fkDef = this.tdef.foreignColumns[prop];
    this.eObj[fkDef.property] = obj;
  }

  handleError(err) {
    console.error(`error adding new ${this.obj.tableName}`);
    console.log(err);

    if (Array.isArray(err.errors)) {
      for (const e of err.errors) {
        this.errors[e.path] = e.message;
        console.error(`${e.path}: ${e.message}`);
      }
    } else {
      throw new Error(err);
    }
  }

  referenceColumns(): IDCReferenceColumnDef[] {
    return Object.values(this.tdef.referenceColumns);
  }

  titleName() {
    if (this.newItem) {
      return `New ${this.tdef.label1}`;
    }

    return this.obj.name;
  }

  updateItem() {
    this.ds.updateItem(this.eObj).subscribe(
      () => {},
      (err) => { this.handleError(err); },
      () => {
        this.completed.emit(DCApiOperation.update);
      });
  }


}
