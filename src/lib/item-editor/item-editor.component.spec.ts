import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEditorComponent } from './item-editor.component';
import {TestingModule} from '../../testing/testing-module';
import {DataServiceStub} from '../../testing/data-service-stub';
import {DataService} from '../data.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {Component, Input} from '@angular/core';

@Component({selector: 'dcd-foreign-object-selector', template: ''})
class ForeignObjectSelectorStubComponent {
  @Input() tableName;
  @Input() property;
  @Input() value;
}

describe('ItemEditorComponent', () => {
  let component: ItemEditorComponent;
  let fixture: ComponentFixture<ItemEditorComponent>;

  beforeEach(async(() => {
    const ds = new DataServiceStub();
    const locationSpy = jasmine.createSpyObj('Location', ['back']);
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [ ItemEditorComponent, ForeignObjectSelectorStubComponent ],
      imports: [ TestingModule ],
      providers: [
        { provide: DataService, useValue: ds },
        { provide: Location, useValue: locationSpy },
        { provide: Router, useValue: routerSpy}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEditorComponent);
    component = fixture.componentInstance;
    component.item$ = DataServiceStub.getSampleItemObservable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
