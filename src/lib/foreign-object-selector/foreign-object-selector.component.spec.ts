import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ForeignObjectSelectorComponent } from './foreign-object-selector.component';
import {DataServiceStub} from '../../testing/data-service-stub';
import {DataService} from '../data.service';
import {TestingModule} from '../../testing/testing-module';


describe('ForeignObjectSelectorComponent', () => {
  let component: ForeignObjectSelectorComponent;
  let fixture: ComponentFixture<ForeignObjectSelectorComponent>;
  const ds = new DataServiceStub();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForeignObjectSelectorComponent ],
      imports: [ TestingModule ],
      providers: [{provide: DataService, useValue: ds}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeignObjectSelectorComponent);
    component = fixture.componentInstance;
    component.property = 'vmHost';
    component.tableName = 'Machine';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
