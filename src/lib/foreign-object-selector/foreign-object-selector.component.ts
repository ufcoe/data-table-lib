import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DCSerializable, IDCTableDef} from '@devctrl/data-model';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {DataService} from '../data.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'dcd-foreign-object-selector',
  templateUrl: './foreign-object-selector.component.html',
  styleUrls: ['./foreign-object-selector.component.css']
})
export class ForeignObjectSelectorComponent implements OnInit {

  data: DCSerializable[] = [];
  data$: Observable<DCSerializable[]>;
  filteredOptions: Observable<DCSerializable[]>;
  inputUpdates = new BehaviorSubject<string>('');
  @Input() property: string;
  @Input() tableName: string;
  @Input() value: DCSerializable;
  @Input() fieldError: string;
  @Output() selected = new EventEmitter<DCSerializable>();
  tDef: IDCTableDef;

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.tDef = this.ds.schema[this.tableName];
    this.data$ = this.ds.getTableObservableArray(this.tableName);
    this.data$.subscribe(data => {
      this.data = data;

      if (data.length === 1) {
        // Select this item
        this.selected.emit(data[0]);
      }
    });

    this.filteredOptions = combineLatest(this.data$, this.inputUpdates).pipe(
      map(([data, inputVal], index) => {
        const val = inputVal.toLowerCase();
        return data.filter(obj => obj.name.toLowerCase().includes(val));
      })
    );
  }

  displayValue(obj) {
    if (typeof obj === 'string') {
      return obj;
    }
    if (obj) { return obj.name; }
    return '';
  }


  initialValue() {
    if (this.value) {
      return this.value.name;
    }

    return '';
  }

  updateValue(obj: DCSerializable | string) {
    if (typeof obj === 'string') {
      console.log(`input value updated: ${obj} entered`);
      this.inputUpdates.next(obj);
    } else {
      console.log(`input value updated: object ${obj.name} selected`);
      this.selected.emit(obj);
    }
  }

}
