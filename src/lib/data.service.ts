import { Injectable } from '@angular/core';
import {DCWebSocketDataService, IDCSerializableClass, IWebSocket} from '@devctrl/data-model';
import {PutersModelList} from '@ufcoe/puters-model';


export class DataServiceConfig {
  websocketUrl: string;
  modelList: IDCSerializableClass[];
  modelVersion: string;
}


@Injectable({
  providedIn: 'root'
})
export class DataService extends DCWebSocketDataService {
  webSocketUrl: string;

  constructor(config: DataServiceConfig) {
    super(config.modelList, config.modelVersion, new WebSocket(config.websocketUrl));

    this.throwErrors = true; // TODO: figure out what to do with errors
    this.webSocketUrl = config.websocketUrl;
  }

  protected socketSetup(socket: IWebSocket) {
    super.socketSetup(socket);

    socket.addEventListener('close', event => {
      setTimeout(() => {
        // console.log(`attempting WebSocket reconnect`);
        this.socketSetup(new WebSocket(this.webSocketUrl));
      }, 2000);
    });
  }
}
