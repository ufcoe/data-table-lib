import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEditorContainerComponent } from './item-editor-container.component';
import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {DCSerializable} from '@devctrl/data-model';
import {ActivatedRouteStub} from '../../testing/activated-route-stub';
import {DataServiceStub} from '../../testing/data-service-stub';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';
import {TestingModule} from '../../testing/testing-module';

@Component({selector: 'dcd-item-editor', template: ''})
class ItemEditorStubComponent {
  @Input() item$: Observable<DCSerializable>;
}


describe('ItemEditorContainerComponent', () => {
  let component: ItemEditorContainerComponent;
  let fixture: ComponentFixture<ItemEditorContainerComponent>;

  beforeEach(async(() => {
    const route = new ActivatedRouteStub({id: 'dfcd27ac-261b-4702-8ec7-08a9b0cfaafb'});
    const ds = new DataServiceStub();

    TestBed.configureTestingModule({
      declarations: [ ItemEditorContainerComponent, ItemEditorStubComponent ],
      imports: [ TestingModule ],
      providers: [
        { provide: DataService, useValue: ds},
        { provide: ActivatedRoute, useValue: route}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEditorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
