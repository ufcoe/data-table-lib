import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {DCApiOperation, DCSerializable} from '@devctrl/data-model';
import {DataService} from '../data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {distinctUntilChanged, filter, flatMap, distinctUntilKeyChanged, map, share, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'dcd-item-editor-container',
  templateUrl: './item-editor-container.component.html',
  styleUrls: ['./item-editor-container.component.css']
})
export class ItemEditorContainerComponent implements OnInit, OnDestroy {

  obj$: Observable<DCSerializable>;
  obj: DCSerializable;
  onDestroy$ = new Subject<void>();


  constructor(private ds: DataService,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location) { }

  ngOnInit() {
    this.obj$ = this.route.params.pipe(
      distinctUntilKeyChanged('id'),
      flatMap(params => this.ds.getItemMatches(params.id)),
      filter( matches => matches.length === 1),
      map( matches => matches[0]),
      distinctUntilChanged(),
      flatMap( match => this.ds.getItemObservable(match)),
      share(),
      takeUntil(this.onDestroy$)
    );

    this.obj$.subscribe(obj => {
      this.obj = obj;
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  editCompleted(op: DCApiOperation) {
    if (op === DCApiOperation.delete) {
      console.log(`item ${this.obj._id} deleted. navigating to data table`);
      this.router.navigate(['data', this.obj.tableName]);
    } else {
      this.location.back();
    }
  }

}
