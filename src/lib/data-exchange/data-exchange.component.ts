import {Component, Input, OnInit} from '@angular/core';
import {IDCDataExchange, IDCSerializable, IDCUpdatePair} from '@devctrl/data-model';
import {DataService} from '../data.service';
import {DCUpdatePair} from '@devctrl/data-model/DCUpdatePair';

//TODO: show more info in status line

@Component({
  selector: 'dcd-data-exchange',
  templateUrl: './data-exchange.component.html',
  styleUrls: ['./data-exchange.component.css']
})
export class DataExchangeComponent implements OnInit {

  @Input() de: IDCDataExchange;
  itemCount: number;
  singleUpdatePair: DCUpdatePair;
  singleObj: IDCSerializable;
  singleTable: string;

  constructor(private ds: DataService) {
  }

  ngOnInit() {
    this.itemCount = this.countItems();

    if (this.itemCount === 1) {
      this.singleTable = Object.keys(this.de.data)[0];
      const obj = this.de.data[this.singleTable][0];
      this.singleUpdatePair = new DCUpdatePair(obj);
      this.singleObj = this.singleUpdatePair.IDCSerializableOf();
    }
  }

  shortId(guid: string): string {
    return this.ds.getShortId(guid);
  }

  countItems(): number {
    if (typeof this.de.data !== 'object') {
      return 0;
    }

    let count = 0;
    for (const t of Object.values(this.de.data)) {
      count += t.length;
    }

    return count;
  }

  get tableList(): string[] {
    return Object.keys(this.de.data);
  }

  updatePair(obj: IDCSerializable | IDCUpdatePair): DCUpdatePair {
    return new DCUpdatePair(obj);
  }

}
