import {Component, Input, OnInit} from '@angular/core';
import {DCSerializable} from '@devctrl/data-model';
import {Observable} from 'rxjs';

@Component({
  selector: 'dcd-item-link',
  templateUrl: './item-link.component.html',
  styleUrls: ['./item-link.component.css']
})
export class ItemLinkComponent implements OnInit {

  @Input() item$: Observable<DCSerializable>;
  constructor() { }

  ngOnInit() {
  }

}
