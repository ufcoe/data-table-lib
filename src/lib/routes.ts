import {Routes} from '@angular/router';
import {TablesListComponent} from './tables-list/tables-list.component';
import {DataTableContainerComponent} from './data-table-container/data-table-container.component';
import {ItemContainerComponent} from './item-container/item-container.component';
import {ItemEditorContainerComponent} from './item-editor-container/item-editor-container.component';
import {NewItemContainerComponent} from './new-item-container/new-item-container.component';
import {DataExchangeLogComponent} from './data-exchange-log/data-exchange-log.component';

export const dataTableRoutes: Routes = [
  {
    path: 'data',
    children: [
      {
        path: '',
        redirectTo: 'data/tables',
        pathMatch: 'full'
      },
      {
        path: 'tables',
        component: TablesListComponent
      },
      {
        path: ':name',
        component: DataTableContainerComponent
      }
    ]
  },
  {
    path: 'items',
    children: [
      {
        path: '',
        redirectTo: 'data/tables',
        pathMatch: 'full'
      },
      {
        path: ':id',
        component: ItemContainerComponent
      }
    ]
  },
  {
    path: 'edit',
    children: [
      {
        path: '',
        redirectTo: 'data/tables',
        pathMatch: 'full'
      },
      {
        path: ':id',
        component: ItemEditorContainerComponent
      }
    ]
  },
  {
    path: 'exchange-log',
    component: DataExchangeLogComponent
  },
  {
    path: 'new/:table',
    component: NewItemContainerComponent
  },
  {
    path: ':table/:id',
    component: ItemContainerComponent
  }

];
