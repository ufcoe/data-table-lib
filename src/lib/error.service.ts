import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MatDialogState} from '@angular/material';
import {ErrorDialogComponent} from './error-dialog/error-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  public errorList = [];
  dialogRef: MatDialogRef<any>;
  errorsHandled = 0;

  constructor(public dialog: MatDialog) {}

  handleError(error) {
    this.errorList.push(error);
    this.errorsHandled++;

    if (! this.dialogRef || this.dialogRef.getState() === MatDialogState.CLOSED) {
      this.dialogRef = this.dialog.open(ErrorDialogComponent, {
        data: {errors: this.errorList}
      });

      this.dialogRef.afterClosed().subscribe(() => {
        console.log(`error dialog closed, clearing ${this.errorList.length} errors`);
        this.errorList.length = 0;
      });
    }

    console.error(`ERROR HANDLED (${this.errorsHandled})`);
    console.log(error);
  }
}
