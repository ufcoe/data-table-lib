import { Injectable } from '@angular/core';
import {ReplaySubject, Observable, Subject} from 'rxjs';
import {DCSerializable} from '@devctrl/data-model';
import {DataService} from './data.service';
import {ActivatedRoute, ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemResolverService {

  partialId: string;
  obj$: Observable<DCSerializable>;
  matches: string[] = [];
  private itemSub;


  constructor(private ds: DataService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DCSerializable> {
    const id = route.paramMap.get('id');

    const match$ = new ReplaySubject<DCSerializable>();
    let matched = false;
    let loaded = false;

    this.ds.getItemMatches(id).subscribe(
      matches => {
        if (matches.length > 1) {
          // multiple matches, navigate to a page that can handle it
          console.log(`multiple matches, redirecting to /items/${id}`);
          this.router.navigate(['/items', id]);
        } else if (matches.length === 1 && !matched) {
          matched = true;
          this.ds.fetchItemRecursive(matches[0]).subscribe(
            obj => {
              if (obj && ! loaded) {
                loaded = true;
                console.log(`item ${id} found and loaded`);
                match$.next(obj);
                match$.complete();
              }
            },
            err => {
              match$.error(err);
            },
            () => {
              if (matched && ! match$.closed) {
                match$.error(`failed to fetch matched item ${matches[0]}`);
              }
            }
          );
        }
      },
      err => {
        match$.error(err);
      },
      () => {
        if (! matched) {
          console.log(`no item matches, redirecting to /items/${id}`);
          this.router.navigate(['/items', id]);
        }
      }
    );

    return match$;
  }
}
