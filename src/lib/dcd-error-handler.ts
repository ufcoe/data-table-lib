import {ErrorHandler, Injectable, NgZone} from '@angular/core';
import {MatDialog, MatDialogRef, MatDialogState} from '@angular/material/dialog';
import {ErrorDialogComponent} from './error-dialog/error-dialog.component';
import {ErrorService} from './error.service';

@Injectable()
export class DcdErrorHandler implements ErrorHandler {


  constructor(public es: ErrorService) {
  }


  handleError(error) {
    this.es.handleError(error);
  }


}
