import {Component, Input, OnInit} from '@angular/core';
import {DCUpdatePair} from '@devctrl/data-model/DCUpdatePair';
import {IDCSerializable} from '@devctrl/data-model';
import {DataService} from '../data.service';

//TODO: show names for foreign object keys



@Component({
  selector: 'dcd-update-pair',
  templateUrl: './update-pair.component.html',
  styleUrls: ['./update-pair.component.css']
})
export class UpdatePairComponent implements OnInit {

  @Input() up: DCUpdatePair;
  obj: IDCSerializable;

  constructor(private ds: DataService) { }

  ngOnInit() {
    this.obj = this.up.IDCSerializableOf();
  }

  get objKeys(): string[] {
    return Object.keys(this.obj);
  }

  get objShortId() {
    return this.ds.getShortId(this.obj._id);
  }

  refObj$(guid: string) {
    return this.ds.fetchItem(guid);
  }

  isGuid(val: any): boolean {
    if (typeof val !== 'string') {
      return false;
    }

    if (val.length !== 36) {
      return false;
    }

    return /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(val);
  }

}
