import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePairComponent } from './update-pair.component';

describe('UpdatePairComponent', () => {
  let component: UpdatePairComponent;
  let fixture: ComponentFixture<UpdatePairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
