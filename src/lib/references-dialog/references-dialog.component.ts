import {Component, Inject, OnInit} from '@angular/core';
import {DCSerializable} from '@devctrl/data-model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';


export interface ReferencesDialogData {
  item: DCSerializable;
  property: string;
}


@Component({
  selector: 'dcd-references-dialog',
  templateUrl: './references-dialog.component.html',
  styleUrls: ['./references-dialog.component.css']
})
export class ReferencesDialogComponent implements OnInit {
  refs: DCSerializable[];


  constructor(public dialogRef: MatDialogRef<ReferencesDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ReferencesDialogData) { }

  ngOnInit() {
    this.refs = this.data.item[this.data.property];
  }

  close() {
    this.dialogRef.close();
  }

}
