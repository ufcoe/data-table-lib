
import {fakeAsync, ComponentFixture, TestBed, tick} from '@angular/core/testing';

import { DataTableComponent } from './data-table.component';
import {RouterLinkStubDirective} from '../../testing/router-link-directive-stub';
import {DataServiceStub} from '../../testing/data-service-stub';
import {DataService} from '../data.service';
import {MaterialStubsModule} from '../../testing/material-stubs';


describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(fakeAsync(() => {
    const ds = new DataServiceStub();

    TestBed.configureTestingModule({
      imports: [
        MaterialStubsModule
      ],
      declarations: [
        DataTableComponent,
        RouterLinkStubDirective
      ],
      providers: [{ provide: DataService, useValue: ds}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    component.tableName = 'Machine';
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
