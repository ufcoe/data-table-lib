import {DataSource} from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort, MatSortable, Sort} from '@angular/material/sort';
import {map, share, startWith, switchMap, tap} from 'rxjs/operators';
import {combineLatest, merge, Observable} from 'rxjs';
import {DCSerializable, ColumnTypes, IDCTableDef} from '@devctrl/data-model';
import {DataService} from '../data.service';


export interface IDCDataTableData {
  def: IDCTableDef;
  data: DCSerializable[];
}


/**
 * Data source for the DataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class DataTableDataSource extends DataSource<DCSerializable> {
  data$: Observable<DCSerializable[]>;


  constructor(private tableData$: Observable<IDCDataTableData>,
              private paginator: MatPaginator,
              private sort$: Observable<Sort>,
              private searchTerm$: Observable<string>) {
    super();
    console.log(`new DataTableDataSource created`);

    //TODO: store page size on local storage
    //TODO: get page index from route
    const page$ = paginator.page.pipe(startWith({
      length: 20,
      pageIndex: 0,
      pageSize: 20,
      previousPageIndex: 0
    }));

    this.data$ = combineLatest(
      tableData$.pipe(tap(val => { /** console.log("DTDS 1"); **/})),
      sort$.pipe(tap(val => { /**console.log("DTDS 2"); */})),
      page$.pipe(tap(val => { /**console.log("DTDS 3"); */})),
      searchTerm$.pipe(tap(val => { /**console.log("DTDS 4"); */}))
    ).pipe(
      //map(([ sortVal ], idx) => {
      map(([tableData, sortVal, page, searchTerm], idx) => {
        const tableName = tableData.def.name;
        const data = tableData.data;

        console.log(`data table update`);
        console.log(
          {
            tableName: tableName,
            data: data,
            sortVal: sortVal,
            page: page,
            searchTerm: searchTerm,
          }
        );

        //tableName and data can get out of sync, check for this condition
        if (data[0] && data[0].tableName !== tableName) {
          console.log(`tableName is ${tableName} but data are from ${data[0].tableName}.  hold off for now`);
          return [];
        }

        const filtered = this.filterData(data, searchTerm, tableData.def);
        paginator.length = filtered.length;
        const sorted = this.getSortedData(filtered, tableData.def, sortVal);
        let startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        startIndex = startIndex > filtered.length ? filtered.length - this.paginator.pageSize : startIndex;
        startIndex = startIndex < 0 ? 0 : startIndex;

        const paged = sorted.slice(startIndex, startIndex + this.paginator.pageSize);
        //console.log(`DDDDDD(6)  emitting paged ${tableName}, sort is ${sortVal.active} - ${sortVal.direction}`);
        return paged;
      }),
      share()
    );
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<DCSerializable[]> {
    console.log(`DataTAbleDatasource.connect()`);
    return this.data$;
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  private filterData(data: DCSerializable[], term, tableDef: IDCTableDef): DCSerializable[] {
    const matches = [];
    const terms = term.toLowerCase().split(' ');

    for (const item of data) {
      let matched = false;

      if (matchesTerms(item.id, terms)) { matched = true; } else
      if (matchesTerms(item.name, terms)) { matched = true; }

      if (! matched) {
        for (const col of Object.values(tableDef.columns)) {
          if (!matched && matchesTerms("" + item[col.property], terms)) { matched = true; }
        }
      }

      if (! matched) {
        for (const fk of Object.values(tableDef.foreignColumns)) {
          if (!matched && item[fk.property] && matchesTerms(item[fk.property].name, terms)) {
            matched = true;
          }
        }
      }

      if (matched) {
        matches.push(item);
      }
    }

    return matches;
  }


  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: DCSerializable[], tableDef: IDCTableDef, sortVal: Sort) {
    //console.log('getSortedData');
    const col = sortVal.active;
    if (!col || sortVal.direction === '') {
      return data;
    }

    return data.sort((a, b) => {

      const isAsc = sortVal.direction === 'asc';
      if (tableDef.columns[col]) {
        const colType = tableDef.columns[col].type;
        if (colType === ColumnTypes.int || colType === ColumnTypes.number) {
          return compare(+a[col], +b[col], isAsc);
        } else {
          return compare(a[col], b[col], isAsc);
        }
      } else if (tableDef.foreignColumns[col]) {
        return compare(a[col] ? a[col].name : '', b[col] ? b[col].name : '', isAsc);
      } else if (tableDef.referenceColumns[col] || tableDef.btmColumns[col]) {
        return compare(a[col][0] ? a[col][0].name : '',
          b[col][0] ? b[col][0].name : '',
          isAsc);
      }

      return compare(a.name, b.name, isAsc);
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  if (typeof a === 'string') {
    return (a.toLowerCase() < b.toLowerCase() ? -1 : 1) * (isAsc ? 1 : -1);
  }
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);

}

function matchesTerms(value: string, terms: string[]): boolean {
  const valL = value.toLowerCase();
  for (const part of terms) {
    if (valL.indexOf(part) === -1) {
      return false;
    }
  }

  return true;
}
