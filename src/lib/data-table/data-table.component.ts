import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort, MatSortable, Sort} from '@angular/material/sort';
import {DataTableDataSource, IDCDataTableData} from './data-table-datasource';
import {DataService} from '../data.service';
import {
  IDCBelongsToManyDef,
  IDCColumnDef,
  IDCTableDef,
  IDCForeignColumnDef,
  IDCReferenceColumnDef, DCSerializable
} from '@devctrl/data-model';
import {ItemEditorDialogComponent} from '../item-editor-dialog/item-editor-dialog.component';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {ReferencesDialogComponent} from '../references-dialog/references-dialog.component';
import {map, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'dcd-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource: DataTableDataSource;
  @Input() table$: Observable<string>;
  tableDef: IDCTableDef;
  fcArray: IDCForeignColumnDef[];
  colArray: IDCColumnDef[];
  refArray: IDCReferenceColumnDef[];
  btmArray: IDCBelongsToManyDef[];
  displayedColumns: string[];
  searchTerm = new BehaviorSubject('');

  constructor(private ds: DataService, private dialog: MatDialog) {
    //
  }

  ngOnInit() {
    const sort$ = new BehaviorSubject<Sort>({ active: 'name', direction: 'asc'});

    const tableData$: Observable<IDCDataTableData> = this.table$.pipe(
      tap( tableName => {
        // Look up default sort on localStorage
        const sortKey = `sortState.${tableName}`;
        const sortStr = window.localStorage.getItem(sortKey);
        console.log(`read local storage: ${sortKey}:${sortStr}`);

        let sortable: MatSortable = {id: 'name', start: 'asc', disableClear: false};
        if (sortStr) {
          const idDir = sortStr.split(".");
          sortable = {id: idDir[0], start: (idDir[1] as "asc" | "desc"), disableClear: false};
        }

        if (this.sort.active !== sortable.id || this.sort.direction !== sortable.start) {
          console.log(`setting sort to ${sortable.id}:${sortable.start}`);
          this.sort.sort(sortable);
        } else {
          console.log(`leaving sort unchanged  ${this.sort.active}:${this.sort.direction}`);
        }
      }),
      switchMap(tableName => {
        return this.ds.getTableObservableArray(tableName).pipe(
          map( data => {
            return {
              def: this.ds.schema[tableName],
              data: data
            };
          })
        );
      })
    );

    this.dataSource = new DataTableDataSource(tableData$, this.paginator, sort$, this.searchTerm);

    this.table$.subscribe(tableName => {
      this.tableDef = this.ds.schema[tableName];
      this.fcArray = Object.values(this.tableDef.foreignColumns);
      this.colArray = Object.values(this.tableDef.columns);
      this.refArray = Object.values(this.tableDef.referenceColumns);
      this.btmArray = Object.values(this.tableDef.btmColumns);

      this.displayedColumns = (["id", "name"])
        .concat(
          this.colArray.map(c => c.property),
          this.fcArray.map(c => c.property),
          this.refArray.map(c => c.property),
          this.btmArray.map(c => c.property),
          "editItem"
        );

      // Reset values on table change
      if (this.paginator.pageIndex) {
        this.paginator.firstPage();
      }

      if (this.searchTerm.getValue()) {
        this.searchTerm.next("");
      }
    });


    this.sort.initialized.subscribe(() => {
      console.log(`sort initialized`);
      sort$.next({ active: this.sort.active, direction: this.sort.direction});
    });

    // Store changes to sort on local storage
    let sortCount = 0;
    this.sort.sortChange.subscribe( sortVal => {
      sortCount++;
      console.log(`sort.sortChange emitted ${sortCount}`);
      console.log(sortVal);

      const sortKey = `sortState.${this.tableDef.name}`;
      const sortStr = `${sortVal.active}.${sortVal.direction}`;
      const oVal = window.localStorage.getItem(sortKey);

      if (oVal !== sortStr) {
          console.log(`writing local storage: ${sortKey}:${sortStr}`);
          window.localStorage.setItem(sortKey, sortStr);
      }

      sort$.next(sortVal);
    });
  }

  ngOnChanges(changes) {
    //if (changes.tableName.previousValue !== changes.tableName.currentValue) {
      //this.ngOnInit();
    //}
  }

  editObj(obj) {
    this.dialog.open(ItemEditorDialogComponent, {
        data: { obj$: this.ds.getItemObservable(obj.id, obj.tableName), newItem: false }
      });
  }


  openRefDialog(obj: DCSerializable, prop: string) {
    this.dialog.open(ReferencesDialogComponent,
      {
        data: {
          item: obj,
          property: prop
        }
      });
  }
}
