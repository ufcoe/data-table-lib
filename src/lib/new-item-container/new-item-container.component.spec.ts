import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewItemContainerComponent } from './new-item-container.component';
import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {DCSerializable} from '@devctrl/data-model';
import {ActivatedRouteStub} from '../../testing/activated-route-stub';
import {DataServiceStub} from '../../testing/data-service-stub';
import {TestingModule} from '../../testing/testing-module';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';

@Component({selector: 'dcd-item-editor', template: ''})
class ItemEditorStubComponent {
  @Input() item$: Observable<DCSerializable>;
  @Input() newItem;
}

describe('NewItemContainerComponent', () => {
  let component: NewItemContainerComponent;
  let fixture: ComponentFixture<NewItemContainerComponent>;

  beforeEach(async(() => {
    const route = new ActivatedRouteStub({id: 'dfcd27ac-261b-4702-8ec7-08a9b0cfaafb'});
    const ds = new DataServiceStub();

    TestBed.configureTestingModule({
      declarations: [ NewItemContainerComponent , ItemEditorStubComponent ],
      imports: [ TestingModule ],
      providers: [
        { provide: DataService, useValue: ds},
        { provide: ActivatedRoute, useValue: route}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewItemContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
