import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../data.service';
import {Observable, of} from 'rxjs';
import {DCApiOperation, DCSerializable} from '@devctrl/data-model';
import {Location} from '@angular/common';

@Component({
  selector: 'dcd-new-item-container',
  templateUrl: './new-item-container.component.html',
  styleUrls: ['./new-item-container.component.css']
})
export class NewItemContainerComponent implements OnInit {

  obj$: Observable<DCSerializable>;
  load = false;

  constructor(private ds: DataService, private route: ActivatedRoute,
              private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.table) {
        this.obj$ = of(this.ds.getNewItem(params.table));
        this.load = true;
      }
    });

    console.log(`init new item container`);
  }

  editCompleted(op: DCApiOperation) {
    this.location.back();
  }
}
