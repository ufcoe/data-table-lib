import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataExchangeLogComponent } from './data-exchange-log.component';

describe('DataExchangeLogComponent', () => {
  let component: DataExchangeLogComponent;
  let fixture: ComponentFixture<DataExchangeLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataExchangeLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataExchangeLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
