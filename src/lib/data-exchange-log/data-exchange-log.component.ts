import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {IDCDataExchange} from '@devctrl/data-model';

@Component({
  selector: 'dcd-data-exchange-log',
  templateUrl: './data-exchange-log.component.html',
  styleUrls: ['./data-exchange-log.component.css']
})
export class DataExchangeLogComponent implements OnInit {

  constructor(private ds: DataService) { }

  ngOnInit() {
  }

  get deLog():  IDCDataExchange[] {
    return this.ds.completedExchanges;
  }
}
