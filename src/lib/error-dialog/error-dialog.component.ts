import {Component, Inject, OnInit, ErrorHandler} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {DcdErrorHandler} from '../dcd-error-handler';
import {ErrorService} from '../error.service';

export interface ErrorDialogData {
  errors: any[];
}


@Component({
  selector: 'dcd-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.css']
})
export class ErrorDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ErrorDialogComponent>,
              public es: ErrorService) {
    console.log(`error dialog created`);
  }

  ngOnInit() {
    console.log(`error dialog opened`);
  }

  close() {
    console.log(`dey close?!?`);
  }

}
