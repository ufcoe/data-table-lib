import { DataTableLibModule } from './data-table-lib.module';

describe('DataTableLibModule', () => {
  let dataTableModule: DataTableLibModule;

  beforeEach(() => {
    dataTableModule = new DataTableLibModule();
  });

  it('should create an instance', () => {
    expect(dataTableModule).toBeTruthy();
  });
});
