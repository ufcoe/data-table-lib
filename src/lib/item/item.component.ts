import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {
  DCSerializable,
  IDCBelongsToManyDef,
  IDCColumnDef,
  IDCForeignColumnDef,
  IDCReferenceColumnDef,
  IDCTableDef
} from '@devctrl/data-model';
import {DataService} from '../data.service';
import {Observable, of} from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {ItemEditorDialogComponent} from '../item-editor-dialog/item-editor-dialog.component';
import {ReferencesDialogComponent} from '../references-dialog/references-dialog.component';

@Component({
  selector: 'dcd-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit, OnChanges {

  values = Object.values;

  @Input() item$: Observable<DCSerializable>;
  obj: DCSerializable;
  tdef: IDCTableDef;

  constructor(private ds: DataService, private dialog: MatDialog) {

  }

  ngOnInit() {
    this.item$.subscribe( obj => {
      this.obj = obj;
      this.tdef = this.ds.schema[obj.tableName];
    });
  }

  ngOnChanges(changes) {
    if (changes.item$.previousValue !== changes.item$.currentValue) {
      this.ngOnInit();
    }
  }

  addBtmObj(refProp: string) {
    const refCol = this.tdef.btmColumns[refProp];
    const newItem = this.ds.getNewItem(refCol.throughTable);
    newItem[refCol.thisProperty] = this.obj;

    this.dialog.open(ItemEditorDialogComponent, {
      data: { obj$: of(newItem), newItem: true }
    });
  }

  // TODO: provide interface to remove btmObj

  btmColumns(): IDCBelongsToManyDef[] {
    return Object.values(this.tdef.btmColumns);
  }

  columns(): IDCColumnDef[] {
    return Object.values(this.tdef.columns);
  }

  /**
   * Edit a BTM relationship.  This requires looking up an object on the "through" table
   * @param col  The BTM column definition
   * @param refObj The referenced object
   */
  editBtmObj(col: IDCBelongsToManyDef, refObj: DCSerializable) {
    const throughObjs = this.ds.getTable(col.throughTable);

    const matchingThroughObjs = Object.values(throughObjs).filter(
      obj => this.obj.equals(obj[col.thisProperty]) && refObj.equals(obj[col.targetProperty])
    );

    if (matchingThroughObjs.length === 1) {
      const obj = matchingThroughObjs[0];
      this.dialog.open(ItemEditorDialogComponent, {
        data: { obj$: this.ds.getItemObservable(obj.id, obj.tableName), newItem: false }
      });
    } else if (matchingThroughObjs.length === 0) {
      throw new Error(`Application Error: editBtmObj, no match found on ${col.throughTable}`);
    } else {
      throw new Error(`Application Error: editBtmObj, multiple matches found on ${col.throughTable}`);
    }
  }

  foreignColumns(): IDCForeignColumnDef[] {
    return Object.values(this.tdef.foreignColumns);
  }

  openRefDialog(prop: string) {
    this.dialog.open(ReferencesDialogComponent,
      {
        data: {
          item: this.obj,
          property: prop
        }
      });
  }

  referenceColumns(): IDCReferenceColumnDef[] {
    return Object.values(this.tdef.referenceColumns);
  }

}
