import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';
import {DataServiceStub} from '../../testing/data-service-stub';
import {TestingModule} from '../../testing/testing-module';
import {DataService} from '../data.service';
import { MatDialog } from '@angular/material/dialog';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async(() => {
    const ds = new DataServiceStub();
    const dialog = jasmine.createSpyObj('MatDialog', ['open']);

    TestBed.configureTestingModule({
      declarations: [ ItemComponent ],
      imports: [ TestingModule ],
      providers: [
        {provide: DataService, useValue: ds},
        {provide: MatDialog, useValue: dialog}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.item$ = DataServiceStub.getSampleItemObservable();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
