import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {DCSerializable} from '@devctrl/data-model';
import {Observable} from 'rxjs';

export interface ItemEditorDialogData {
  obj$: Observable<DCSerializable>;
  newItem?: boolean;
}

@Component({
  selector: 'dcd-item-editor-dialog',
  templateUrl: './item-editor-dialog.component.html',
  styleUrls: ['./item-editor-dialog.component.css']
})
export class ItemEditorDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ItemEditorDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ItemEditorDialogData) {

  }

  ngOnInit() {
  }

  editCompleted(status) {
    this.dialogRef.close();
  }
}
