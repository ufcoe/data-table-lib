import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {ItemEditorDialogComponent, ItemEditorDialogData} from './item-editor-dialog.component';
import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {DCSerializable} from '@devctrl/data-model';
import {TestingModule} from '../../testing/testing-module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {DataServiceStub} from '../../testing/data-service-stub';

@Component({selector: 'dcd-item-editor', template: ''})
class ItemEditorStubComponent {
  @Input() item$: Observable<DCSerializable>;
  @Input() newItem;
}

describe('ItemEditorDialogComponent', () => {
  let component: ItemEditorDialogComponent;
  let fixture: ComponentFixture<ItemEditorDialogComponent>;

  beforeEach(async(() => {
    const data: ItemEditorDialogData = {
      obj$: DataServiceStub.getSampleItemObservable()
    };

    TestBed.configureTestingModule({
      declarations: [ ItemEditorStubComponent, ItemEditorDialogComponent ],
      imports: [ TestingModule ],
      providers: [
        { provide: MatDialogRef, useValue: {}},
        { provide: MAT_DIALOG_DATA, useValue: data}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEditorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
