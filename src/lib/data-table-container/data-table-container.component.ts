import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'dcd-data-table-container',
  templateUrl: './data-table-container.component.html',
  styleUrls: ['./data-table-container.component.css']
})
export class DataTableContainerComponent implements OnInit {
  table$: Observable<string>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.table$ = this.route.params.pipe(map(params => params.name));
  }

}
