import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableContainerComponent } from './data-table-container.component';
import {ActivatedRouteStub} from '../../testing/activated-route-stub';
import {MaterialStubsModule} from '../../testing/material-stubs';
import {ActivatedRoute} from '@angular/router';
import {Component, Input} from '@angular/core';

@Component({ selector: 'dcd-data-table', template: ''})
class DataTableStubComponent {
  @Input() tableName;
}


describe('DataTableContainerComponent', () => {
  let component: DataTableContainerComponent;
  let fixture: ComponentFixture<DataTableContainerComponent>;

  beforeEach(async(() => {
    const route = new ActivatedRouteStub({ name: 'machines'});
    TestBed.configureTestingModule({
      declarations: [
        DataTableContainerComponent,
        DataTableStubComponent
      ],
      imports: [
        MaterialStubsModule
      ],
      providers: [ { provide: ActivatedRoute, useValue: route} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(DataTableContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
