import {ErrorHandler, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ItemComponent} from './item/item.component';
import {DataTableComponent} from './data-table/data-table.component';
import {DataTableContainerComponent} from './data-table-container/data-table-container.component';
import {ForeignObjectSelectorComponent} from './foreign-object-selector/foreign-object-selector.component';
import {ItemContainerComponent} from './item-container/item-container.component';
import {ItemEditorComponent} from './item-editor/item-editor.component';
import {ItemEditorContainerComponent} from './item-editor-container/item-editor-container.component';
import {ItemEditorDialogComponent} from './item-editor-dialog/item-editor-dialog.component';
import {TablesListComponent} from './tables-list/tables-list.component';
import {NewItemContainerComponent} from './new-item-container/new-item-container.component';
import {dataTableRoutes} from './routes';
import {DCDMaterialModule} from './dcdmaterial.module';
import {DataService, DataServiceConfig} from './data.service';
import {ItemResolverService} from './item-resolver.service';
import {ErrorDialogComponent} from './error-dialog/error-dialog.component';
import {DcdErrorHandler} from './dcd-error-handler';
import {ReferencesDialogComponent} from './references-dialog/references-dialog.component';
import { UpdateSetViewerComponent } from './update-set-viewer/update-set-viewer.component';
import { DataExchangeComponent } from './data-exchange/data-exchange.component';
import { DataExchangeLogComponent } from './data-exchange-log/data-exchange-log.component';
import { UpdatePairComponent } from './update-pair/update-pair.component';
import { ItemLinkComponent } from './item-link/item-link.component';
import {ErrorService} from './error.service';

@NgModule({
  imports: [
    CommonModule,
    DCDMaterialModule,
    FormsModule,
    RouterModule.forChild(dataTableRoutes)
  ],
  declarations: [
    DataTableComponent,
    DataTableContainerComponent,
    ErrorDialogComponent,
    ForeignObjectSelectorComponent,
    ItemComponent,
    ItemContainerComponent,
    ItemEditorComponent,
    ItemEditorContainerComponent,
    ItemEditorDialogComponent,
    NewItemContainerComponent,
    ReferencesDialogComponent,
    TablesListComponent,
    UpdateSetViewerComponent,
    DataExchangeComponent,
    DataExchangeLogComponent,
    UpdatePairComponent,
    ItemLinkComponent,
  ],
  entryComponents: [
    ErrorDialogComponent,
    ItemEditorDialogComponent,
    ReferencesDialogComponent
  ],
  exports: [
    DataTableComponent,
    DataTableContainerComponent,
    ErrorDialogComponent,
    ForeignObjectSelectorComponent,
    ItemComponent,
    ItemContainerComponent,
    ItemEditorComponent,
    ItemEditorContainerComponent,
    ItemEditorDialogComponent,
    NewItemContainerComponent,
    RouterModule,
    TablesListComponent,
    UpdateSetViewerComponent,
    DataExchangeComponent,
    DataExchangeLogComponent,
    UpdatePairComponent,
    ItemLinkComponent
  ],
  providers: [
    DataService,
    ItemResolverService,
    ErrorService,
    {provide: ErrorHandler, useClass: DcdErrorHandler}]
})
export class DataTableLibModule {
  static forRoot(config: DataServiceConfig): ModuleWithProviders {
    return {
      ngModule: DataTableLibModule,
      providers: [
        {provide: DataServiceConfig, useValue: config }
      ]
    };
  }
}
